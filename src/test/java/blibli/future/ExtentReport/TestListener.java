package blibli.future.ExtentReport;

import blibli.future.AppTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.io.FileHandler;
import org.testng.*;
import com.aventstack.extentreports.Status;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TestListener implements ITestListener {


    public void onStart(ITestContext context) {
        System.out.println("*** Test Suite " + context.getName() + " started ***");
    }

    public void onFinish(ITestContext context) {
        System.out.println(("*** Test Suite " + context.getName() + " ending ***"));
        ExtendTestManager.endTest();
        ExtentManager.getInstance().flush();
    }

    public void onTestStart(ITestResult result) {
        System.out.println(("*** Running test method " + result.getMethod().getMethodName() + "..."));
        ExtendTestManager.startTest(result.getMethod().getMethodName());
    }

    public void onTestSuccess(ITestResult result) {
        System.out.println("*** Executed " + result.getMethod().getMethodName() + " test successfully...");

        Object testClass = result.getInstance();
        WebDriver webDriver = ((AppTest) testClass).getDriver();

        String imageLocation = null;
        String targetLocation = null;

        String testMethodName = result.getMethod().getMethodName();
        String screenShotName = testMethodName + ".png";
        String fileSeperator = System.getProperty("file.separator");
        String reportsPath = System.getProperty("user.dir") + fileSeperator + "TestReport" + fileSeperator
                + "screenshots";
        System.out.println("Screen shots reports path - " + reportsPath);
        try {
            File file = new File(reportsPath + fileSeperator); // Set

            if (!file.exists()) {
                if (file.mkdirs()) {
                    System.out.println("Directory: " + file.getAbsolutePath() + " is created!");
                } else {
                    System.out.println("Failed to create directory: " + file.getAbsolutePath());
                }

            }
            File screenshotFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
            // location
            targetLocation = reportsPath + fileSeperator + fileSeperator + screenShotName;
            File targetFile = new File(targetLocation);
            imageLocation = "screenshots"  + fileSeperator + fileSeperator + screenShotName;
            System.out.println("Screen shot file location - " + screenshotFile.getAbsolutePath());
            System.out.println("Target File location - " + targetFile.getAbsolutePath());
            FileHandler.copy(screenshotFile, targetFile);

        } catch (FileNotFoundException e) {
            System.out.println("File not found exception occurred while taking screenshot " + e.getMessage());
        } catch (Exception e) {
            System.out.println("An exception occurred while taking screenshot " + e.getCause());
        }

        // attach screenshots to report
        try {
            ExtendTestManager.getTest().pass("Result",
                    MediaEntityBuilder.createScreenCaptureFromPath(imageLocation).build());
        } catch (IOException e) {
            System.out.println("An exception occured while taking screenshot " + e.getCause());
        }
        ExtendTestManager.getTest().log(Status.PASS, "Test passed");
    }

    public void onTestFailure(ITestResult result) {
        System.out.println("*** Test execution " + result.getMethod().getMethodName() + " failed...");
        System.out.println((result.getMethod().getMethodName() + " failed!"));

        Object testClass = result.getInstance();
        WebDriver webDriver = ((AppTest) testClass).getDriver();

        String imageLocation = null;
        String targetLocation = null;

        String testMethodName = result.getMethod().getMethodName();
        String screenShotName = testMethodName + ".png";
        String fileSeperator = System.getProperty("file.separator");
        String reportsPath = System.getProperty("user.dir") + fileSeperator + "TestReport" + fileSeperator
                + "screenshots";
       System.out.println("Screen shots reports path - " + reportsPath);
        try {
            File file = new File(reportsPath + fileSeperator); // Set
            // screenshots
            // folder
            if (!file.exists()) {
                if (file.mkdirs()) {
                  System.out.println("Directory: " + file.getAbsolutePath() + " is created!");
                } else {
                    System.out.println("Failed to create directory: " + file.getAbsolutePath());
                }

            }

            File screenshotFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
            // location
            targetLocation = reportsPath + fileSeperator + fileSeperator + screenShotName;
            File targetFile = new File(targetLocation);
            imageLocation = "screenshots"  + fileSeperator + fileSeperator + screenShotName;
            System.out.println("Screen shot file location - " + screenshotFile.getAbsolutePath());
            System.out.println("Target File location - " + targetFile.getAbsolutePath());
            FileHandler.copy(screenshotFile, targetFile);

        } catch (FileNotFoundException e) {
            System.out.println("File not found exception occurred while taking screenshot " + e.getMessage());
        } catch (Exception e) {
            System.out.println("An exception occurred while taking screenshot " + e.getCause());
        }
        String message = null;
        if(result.getThrowable() != null){
            message = result.getThrowable().getMessage();
        }

        // attach screenshots to report
        try {
            ExtendTestManager.getTest().fail(message,
                    MediaEntityBuilder.createScreenCaptureFromPath(imageLocation).build());
        } catch (IOException e) {
            System.out.println("An exception occured while taking screenshot " + e.getCause());
        }
        ExtendTestManager.getTest().log(Status.FAIL, "Test Failed");
    }

    public void onTestSkipped(ITestResult result) {
        System.out.println("*** Test " + result.getMethod().getMethodName() + " skipped...");
        ExtendTestManager.getTest().log(Status.SKIP, "Test Skipped");
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
        System.out.println("*** Test failed but within percentage % " + result.getMethod().getMethodName());
    }
}