package blibli.future;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import java.io.FileInputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Properties;

public class TestBase {


    private Properties obj = new Properties();
    private FileInputStream objfile = new FileInputStream(System.getProperty("user.dir")+"\\application.properties");


    Properties getObj() throws IOException {
        obj.load(objfile);
        return obj;
    }

    TestBase() throws IOException {
    }




    protected static ThreadLocal<RemoteWebDriver> webDriver = new ThreadLocal<>();
    private CapabilityFactory capabilityFactory = new CapabilityFactory();

    private String browserName = getObj().getProperty("browser");
    @BeforeMethod
    public void setUp() throws MalformedURLException {
        webDriver.set(new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"),
                capabilityFactory.getCapabilities(browserName)));
    }

    public WebDriver getDriver(){
        return webDriver.get();
    }

    @AfterMethod
    public void tearDown(){
        getDriver().quit();
    }

    @AfterClass
    void terminate(){
        webDriver.remove();
    }

}
