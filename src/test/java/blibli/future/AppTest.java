package blibli.future;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.testng.AssertJUnit.assertTrue;
import org.testng.annotations.*;
import blibli.future.blibliPage.*;
import java.io.IOException;


/**
 * Unit test for simple App.
 */
public class AppTest extends TestBase
{

    private String hotelOrder = getObj().getProperty("hotelOrder");
    private String roomOrder = getObj().getProperty("roomOrder");
    private String dateIn = getObj().getProperty("date-in");
    private String dateOut = getObj().getProperty("date-out");
    private int totalRoom = Integer.parseInt(getObj().getProperty("ruangan"));
    private int totalTamu = Integer.parseInt(getObj().getProperty("tamu"));

    public AppTest() throws IOException {
    }

    /**
     * Rigorous Test :-)
     */

//    1. open blibli.com
//    @Test
//    public void testOpenPage() throws Exception {
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        assertThat(("Open Page salah"), homePage.isImageBlibli());
//    }
//
//    //2. Open hotel page
//    @Test
//    public void testhotelPage() throws Exception {
//        HomePage homePage = new HomePage(getDriver());
//        String hotel = getObj().getProperty("hotelString");
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        assertThat(("Open hotel page salah"), getDriver().getCurrentUrl(), containsString(hotel));
//    }
//
//    @Test
//    public void testHoteldiSingapore() throws Exception {
//        HomePage homePage= new HomePage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        String singapore = getObj().getProperty("singaporeString");
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickSingaporeHotel();
//        String cityName = resultSearchPage.getKota();
//        assertThat("Hotel di Singapore salah", cityName, containsString(singapore));
//    }
//
//    @Test
//    public void testHoteldiKualaLumpur() throws Exception {
//        HomePage homePage= new HomePage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        String kualaLumpur = getObj().getProperty("kualaLumpur");
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickKualalumpurHotel();
//        String cityName = resultSearchPage.getKota();
//        assertThat("Hotel di kuala lumpur salah", cityName, containsString(kualaLumpur));
//    }
//
//    @Test
//    public void testHoteldiTokyo() throws Exception {
//        HomePage homePage= new HomePage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        String tokyo = getObj().getProperty("tokyo");
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickTokyoHotel();
//        String cityName = resultSearchPage.getKota();
//        assertThat("Hotel di tokyo salah", cityName, containsString(tokyo));
//    }
//
//    @Test
//    public void testHoteldiBangkok() throws Exception {
//        HomePage homePage= new HomePage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        String bangkok = getObj().getProperty("bangkok");
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickBangkokHotel();
//        String cityName = resultSearchPage.getKota();
//        assertThat("Hotel di Bangkok salah", cityName, containsString(bangkok));
//    }
//
//    @Test
//    public void testHoteldiJakarta() throws Exception {
//        HomePage homePage= new HomePage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        String jakarta = getObj().getProperty("jakarta");
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickJakartaHotel();
//        String cityName = resultSearchPage.getKota();
//        assertThat("Hotel di jakarta salah", cityName, containsString(jakarta));
//    }
//
//    @Test
//    public void testHoteldiBali() throws Exception {
//        HomePage homePage= new HomePage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        String bali = getObj().getProperty("bali");
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickBaliHotel();
//        String cityName = resultSearchPage.getKota();
//        assertThat("Hotel di bali salah", cityName, containsString(bali));
//    }
//
//    @Test
//    public void testHoteldiSurabaya() throws Exception {
//        HomePage homePage= new HomePage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        String surabaya = getObj().getProperty("surabaya");
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickSurabayaHotel();
//        String cityName = resultSearchPage.getKota();
//        assertThat("Hotel di surabaya salah", cityName, containsString(surabaya));
//    }
//
//    @Test
//    public void testHoteldiYogyakarta() throws Exception {
//        HomePage homePage= new HomePage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        String yogyakarta = getObj().getProperty("yogyakarta");
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickYogyakartaHotel();
//        String cityName = resultSearchPage.getKota();
//        assertThat("Hotel di yogyakarta salah", cityName, containsString(yogyakarta));
//    }
//
//    @Test
//    public void testHoteldiBandung() throws Exception {
//        HomePage homePage= new HomePage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        String bandung = getObj().getProperty("bandung");
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickBandungHotel();
//        String cityName = resultSearchPage.getKota();
//        assertThat("Hotel di bandung salah", cityName, containsString(bandung));
//    }
//
//    //3. Insert city 1
//    @Test
//    public void testCityHotel1() throws Exception {
//        String kota = getObj().getProperty("kota");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.deleteKota(4);
//        hotelPage.insertKota(kota);
//        hotelPage.selectKota();
//        assertThat(("kota salah"), hotelPage.getKotaSelected(), containsString(kota));
//    }
//
//    //4. insert city 2
//    @Test
//    public void testCityHotel2() throws Exception {
//        String kota = getObj().getProperty("kota2");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.deleteKota(4);
//        hotelPage.insertKota(kota);
////        Thread.sleep(1000);
//        assertThat(("Kota 2 salah"), hotelPage.getKotaSelected(), containsString(kota.toLowerCase()));
//    }
////
//    //5. insert date check-in
//    @Test
//    public void testDateIn() throws Exception {
//        String date_in = getObj().getProperty("date-in");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.deleteKota(4);
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(date_in);
//        assertThat(("Date in salah"), hotelPage.getDateInSelected(), containsString(date_in));
//    }
//
//    //6. insert date check-out
//    @Test
//    public void testDateOut() throws Exception {
//        String date_out = getObj().getProperty("date-out");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(date_out);
//        assertThat(("Date out salah"), hotelPage.getDateOutSelected(), containsString(date_out));
//    }
////
//    //7. Test insert total people
//    @Test
//    public void testTotalPeople() throws Exception {
//        int tamu = Integer.parseInt(getObj().getProperty("tamu"));
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickJumlah();
//        hotelPage.clickPlusTamu(tamu);
//        assertThat(("Total people salah"), hotelPage.getTotalPeopleOutput(), containsString(hotelPage.getTotalPeopleInput()));
//    }
////
//    //8. insert total room
//    @Test
//    public void testTotalRoom() throws Exception {
//        int kamar = Integer.parseInt(getObj().getProperty("ruangan"));
//        int tamu = Integer.parseInt(getObj().getProperty("tamu"));
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickJumlah();
//        hotelPage.clickPlusTamu(tamu);
//        hotelPage.clickPlusKamar(kamar);
//        assertThat(("Total room salah"), hotelPage.getTotalRoomOutput(), containsString(hotelPage.getTotalRoomiInput()));
//    }
//
//    @Test
//    public void testTotalRoom2() throws Exception {
//        int kamar = Integer.parseInt(getObj().getProperty("ruangan"));
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickJumlah();
//        hotelPage.clickPlusKamar(kamar);
//        assertThat(("Total People must be greater than total room"), hotelPage.getTotalRoomOutput(), lessThanOrEqualTo(hotelPage.getTotalPeopleOutput()));
//    }
//
////    9. Tes Order Hotel
//    @Test
//    public void testHotelOrdered() throws Exception {
//        String kota = getObj().getProperty("kota");
//        String date_in = getObj().getProperty("date-in");
//        String date_out = getObj().getProperty("date-out");
//        int kamar = Integer.parseInt(getObj().getProperty("ruangan"));
//        int tamu = Integer.parseInt(getObj().getProperty("tamu"));
//        HotelPage hotelPage = new HotelPage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.deleteKota(4);
//        hotelPage.insertKota(kota);
//        hotelPage.selectKota();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(date_in);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(date_out);
//        hotelPage.clickJumlah();
//        hotelPage.clickPlusTamu(tamu);
//        hotelPage.clickPlusKamar(kamar);
//        hotelPage.clickBtnCariHotel();
//        assertThat("Date In Wrong", resultSearchPage.getDateIn(), containsString(date_in));
//        assertThat("City wrong", resultSearchPage.getKota(), containsString(kota));
//        assertThat("Date out wrong", resultSearchPage.getDataOut(), containsString(date_out));
//    }
////
//    @Test
//    public void testHargaTermurah() throws Exception {
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickHargaTermurah();
//        int harga1 = (int) resultSearchPage.getFirstPrice();
//        int harga2 = (int) resultSearchPage.getSecondPrice();
//        int harga3 = (int) resultSearchPage.getThirdPrice();
//        assertThat(("Harga termurah salah"), harga2, greaterThanOrEqualTo(harga1));
//        assertThat(("Harga Termurah salah"), harga3, greaterThanOrEqualTo(harga2));
//    }
//

    @Test
    public void tesHargaTermahal() throws Exception {
        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
        String kota = getObj().getProperty("kota");
        int kamar = Integer.parseInt(getObj().getProperty("ruangan"));
        int tamu = Integer.parseInt(getObj().getProperty("tamu"));
        HotelPage hotelPage = new HotelPage(getDriver());
        HomePage homePage = new HomePage(getDriver());
        homePage.openHomePage();
        homePage.cleanView();
        homePage.clickHotel();
        hotelPage.deleteKota(4);
        hotelPage.insertKota(kota);
        hotelPage.selectKota();
        hotelPage.clickJumlah();
        hotelPage.clickPlusTamu(tamu);
        hotelPage.clickPlusKamar(kamar);
        hotelPage.clickBtnCariHotel();
        resultSearchPage.clickHargaTermahal();
        int harga1 = (int) resultSearchPage.getFirstPrice();
        int harga2 = (int) resultSearchPage.getSecondPrice();
        int harga3 = (int) resultSearchPage.getThirdPrice();
        assertThat(("Harga termahal salah"), harga1, greaterThan(harga2));
        assertThat(("Harga Termurah salah"), harga2, greaterThan(harga3));
    }
//
//    @Test
//    public void tesReviewTerbaik() throws Exception {
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickReviewTerbaik();
//        double rating1 = resultSearchPage.getFirstReview();
//        double rating2 = resultSearchPage.getSecondReview();
//        double rating3 = resultSearchPage.getTenthReview();
//        assertTrue(("Rating salah"), rating1 >= rating2 && rating2 >= rating3);
//    }
////
//    @Test
//    public void testPriceSlider() throws Exception {
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.dragMaxPrice();
//        int maxPrice = (int) resultSearchPage.getMaxPrice();
//        resultSearchPage.clickHargaTermahal();
//        int highestPrice = (int) resultSearchPage.getFirstPrice();
//        assertThat(("Harga Termahal salah"), highestPrice, lessThanOrEqualTo(maxPrice));
//        int minPrice = (int) resultSearchPage.getMinPrice();
//        resultSearchPage.clickResetBtn();
//        resultSearchPage.dragMinPrice();
//        resultSearchPage.clickHargaTermurah();
//        int lowestPrice = (int) resultSearchPage.getFirstPrice();
//        assertThat(("Harga Termurah salah"), lowestPrice, greaterThanOrEqualTo(minPrice));
//
//    }
//
//    @Test
//    public void testPriceSlider2 () throws Exception {
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        int kamar = Integer.parseInt(getObj().getProperty("ruangan"));
//        int tamu = Integer.parseInt(getObj().getProperty("tamu"));
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickJumlah();
//        hotelPage.clickPlusTamu(kamar);
//        hotelPage.clickPlusKamar(tamu);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.dragMaxPrice();
//        int maxPrice = (int) resultSearchPage.getMaxPrice();
//        resultSearchPage.clickHargaTermahal();
//        int highestPrice = (int) resultSearchPage.getFirstPrice();
//        highestPrice = highestPrice * 2;
//        assertThat(("Harga Termahal salah"), highestPrice, lessThanOrEqualTo(maxPrice));
//        resultSearchPage.clickResetBtn();
//        resultSearchPage.dragMinPrice();
//        int minPrice = (int) resultSearchPage.getMinPrice();
//        resultSearchPage.clickHargaTermurah();
//        int lowestPrice = (int) resultSearchPage.getFirstPrice();
//
//        lowestPrice = lowestPrice * 2;
//        assertThat(("Harga Termurah salah"), lowestPrice, greaterThanOrEqualTo(minPrice));
//
//    }
////
//    @Test
//    public void testChooseHotel() throws Exception {
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickBtnCariHotel();
//        String name1  = resultSearchPage.getFirstName(hotelOrder);
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        String name2 = hotelDetailPage.getHotelName();
//        assertThat(("Choose Hotel salah"), name2.toLowerCase(), containsString(name1.toLowerCase()));
//    }
//
//    @Test
//    public void testMemesanKamar() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        String pemesananExpected = getObj().getProperty("pemesananExpected");
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickBtnCariHotel();
//        String hotelName = resultSearchPage.getFirstName(hotelOrder).toLowerCase();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        String roomName = hotelDetailPage.getRoomName(roomOrder).toLowerCase();
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        String title = pemesananPage.getIsiDataTitle();
//        assertThat(("Memesan Kamar salah"), title.toLowerCase(), containsString(pemesananExpected));
//        assertThat("Nama hotel salah", pemesananPage.getHotelName().toLowerCase(), containsString(hotelName));
//        assertThat("Nama Ruangan salah", pemesananPage.getRoomName().toLowerCase(), containsString(roomName));
//    }
//
//
//    @Test
//    public void testInputNamaPemesan() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        String name = getObj().getProperty("name1");
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputName(name);
//        String nameTest = pemesananPage.getName();
//        assertThat(("Input Name salah"), name, equalTo(nameTest));
//    }
//
//    @Test
//    public void testInputNamaMinimal() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String name = getObj().getProperty("name2");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputName(name);
//        String error = getObj().getProperty("errorNameMin");
//        String errorTest = pemesananPage.getErrorName();
//        assertThat(("Input Name salah"), errorTest.toLowerCase(), containsString(error));
//    }
////
//    @Test
//    public void testInputNameNumber() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String name = getObj().getProperty("name3");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputName(name);
//        String error = getObj().getProperty("errorNameNumber");
//        String errorTest = pemesananPage.getErrorName();
//        assertThat(("Input Name salah"), errorTest.toLowerCase(), containsString(error));
//    }
////
//    @Test
//    public void testInputNameChar() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String name = getObj().getProperty("name4");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputName(name);
//        String error = getObj().getProperty("errorNameChar");
//        String errorTest = pemesananPage.getErrorName();
//        assertThat(("Input Name salah"), errorTest.toLowerCase(), containsString(error));
//    }
////
//    @Test
//    public void testInputNameMax() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String name = getObj().getProperty("name5");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputName(name);
//        String error = getObj().getProperty("errorNameMax");
//        String errorTest = pemesananPage.getErrorName();
//        assertThat(("Input Name salah"), errorTest.toLowerCase(), containsString(error));
//    }
////
//    @Test
//    public void testInputTelp() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String telp = getObj().getProperty("telp1");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputTelpNumber(telp);
//        String telpTest = pemesananPage.getTelpNumber();
//        assertThat(("Input Telephone salah"), telp.toLowerCase(), containsString(telpTest));
//    }
//
//    @Test
//    public void testInputTelpMin() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String telp = getObj().getProperty("telp2");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputTelpNumber(telp);
//        String error = getObj().getProperty("errorTelpMin");
//        String errorTest = pemesananPage.getErrorTelp();
//        assertThat(("Input Telephone salah"), errorTest.toLowerCase(), containsString(error));
//    }
////
//    @Test
//    public void testInputTelpFormat() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String telp = getObj().getProperty("telp3");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputTelpNumber(telp);
//        String error = getObj().getProperty("errorTelpFormat");
//        String errorTest = pemesananPage.getErrorTelp();
//        assertThat(("Input Telephone salah"), errorTest.toLowerCase(), containsString(error.toLowerCase()));
//    }
////
//    @Test
//    public void testInputTelpFormat2() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String telp = getObj().getProperty("telp4");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputTelpNumber(telp);
//        String error = getObj().getProperty("errorTelpMax");
//        String errorTest = pemesananPage.getErrorTelp();
//        assertThat(("Input Telephone salah"), errorTest.toLowerCase(), containsString(error.toLowerCase()));
//    }
////
//    @Test
//    public void testInputEmail() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String email = getObj().getProperty("email1");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputEmail(email);
//        String emailTest = pemesananPage.getEmail();
//        assertThat(("Input email salah"), emailTest.toLowerCase(), equalToIgnoringCase(email));
//    }
////
//    @Test
//    public void testInputEmailMin() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String email = getObj().getProperty("email2");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputEmail(email);
//        String error = getObj().getProperty("errorEmailMin");
//        String errorTest = pemesananPage.getErrorEmail();
//        assertThat(("Input email salah"), errorTest.toLowerCase(), containsString(error));
//    }
//
//    @Test
//    public void testInputEmailFormat() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String email = getObj().getProperty("email3");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputEmail(email);
//        String error = getObj().getProperty("errorEmailFormat");
//        String errorTest = pemesananPage.getErrorEmail();
//        assertThat(("Input email salah"), errorTest.toLowerCase(), containsString(error));
//    }
//
//    @Test
//    public void testInputEmailFormat2() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String email = getObj().getProperty("email4");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputEmail(email);
//        String error = getObj().getProperty("errorEmailFormat");
//        String errorTest = pemesananPage.getErrorEmail();
//        assertThat(("Input email salah"), errorTest.toLowerCase(), containsString(error));
//    }
//
//    @Test
//    public void testInputEmailFormat3() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String email = getObj().getProperty("email5");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputEmail(email);
//        String error = getObj().getProperty("errorEmailFormat");
//        String errorTest = pemesananPage.getErrorEmail();
//        assertThat(("Input email salah"), errorTest.toLowerCase(), containsString(error));
//    }
//
//    @Test
//    public void testInputTamu() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String name = getObj().getProperty("namaTamu");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputTamu(name);
//        String nameTest = pemesananPage.getTamu();
//        assertThat(("Input email salah"), name.toLowerCase(), equalToIgnoringCase(nameTest));
//    }
//////
//    @Test
//    public void testInputTamuMin() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String name = getObj().getProperty("namaTamuMin");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputTamu(name);
//        String error = getObj().getProperty("errorTamuMin");
//        String errorTest = pemesananPage.getErrorTamu();
//        assertThat(("Input nama tamu salah"), errorTest.toLowerCase(), containsString(error.toLowerCase()));
//    }
////
//    @Test
//    public void testInputTamuMax() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String name = getObj().getProperty("namaTamuMax");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputTamu(name);
//        String error = getObj().getProperty("errorTamuMax");
//        String errorTest = pemesananPage.getErrorTamu();
//        assertThat(("Input nama tamu salah"), errorTest.toLowerCase(), containsString(error.toLowerCase()));
//    }
////
//    @Test
//    public void testInputTamuFormat() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String name = getObj().getProperty("namaTamu2");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputTamu(name);
//        String error = getObj().getProperty("errorTamuFormat");
//        String errorTest = pemesananPage.getErrorTamu();
//        assertThat(("Input nama tamu salah"), errorTest.toLowerCase(), containsString(error.toLowerCase()));
//    }
//
//    @Test
//    public void testInputTamuFormat2() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String name = getObj().getProperty("namaTamu3");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputTamu(name);
//        String error = getObj().getProperty("errorTamuFormat");
//        String errorTest = pemesananPage.getErrorTamu();
//        assertThat(("Input nama tamu salah"), errorTest.toLowerCase(), containsString(error.toLowerCase()));
//    }
//
//    @Test
//    public void testInputTamuFormat3() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        String name = getObj().getProperty("name1");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputName(name);
//        pemesananPage.clickBtnSamaPemesan();
//        String nameTest = pemesananPage.getTamu();
//        assertThat(("Input nama tamu salah"), nameTest.toLowerCase(), containsString(name.toLowerCase()));
//    }
//
//    @Test
//    public void testLanjutPemesanan() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        PembayaranPage pembayaranPage = new PembayaranPage(getDriver());
//        String name = getObj().getProperty("name1");
//        String telp = getObj().getProperty("telp1");
//        String email = getObj().getProperty("email1");
//        String nameTamu = getObj().getProperty("namaTamu");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickBtnCariHotel();
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputName(name);
//        pemesananPage.inputTelpNumber(telp);
//        pemesananPage.inputEmail(email);
//        pemesananPage.inputTamu(nameTamu);
//        pemesananPage.clickBtnLanjutkan();
//        String pembayaran = pembayaranPage.getPembayaranPage();
//        assertThat(("Input nama tamu salah"), pembayaran.toLowerCase(), containsString("pembayaran"));
//    }
//
//    @Test
//    public void testPembayaranVABank() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        PembayaranPage pembayaranPage = new PembayaranPage(getDriver());
//        LakukanPembayaranPage lakukanPembayaranPage = new LakukanPembayaranPage(getDriver());
//        String name = getObj().getProperty("name1");
//        String telp = getObj().getProperty("telp1");
//        String email = getObj().getProperty("email1");
//        String nameTamu = getObj().getProperty("namaTamu");
//        String bank = getObj().getProperty("bank");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        String kota = hotelPage.getKotaSelected();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickJumlah();
//        hotelPage.clickPlusKamar(totalRoom);
//        hotelPage.clickPlusTamu(totalTamu);
//        hotelPage.clickBtnCariHotel();
//        String hotel = resultSearchPage.getFirstName(hotelOrder);
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        String ruangan = hotelDetailPage.getRoomName(roomOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputName(name);
//        pemesananPage.inputTelpNumber(telp);
//        pemesananPage.inputEmail(email);
//        pemesananPage.inputTamu(nameTamu);
//        pemesananPage.clickBtnLanjutkan();
//        String hargaTotal = pembayaranPage.getTotalPrice();
//        pembayaranPage.clickTFVirtualAccount();
//        pembayaranPage.clickOpsiPembayaran();
//        pembayaranPage.clickBankVA(bank);
//        pembayaranPage.clickBtnBayar();
//        String namaBank = lakukanPembayaranPage.getPembayaran();
//        String nameMetode = lakukanPembayaranPage.getMetodePembayaran();
//
//        assertThat("Nama Bank pembayaran salah", namaBank.toLowerCase(), containsString(bank.toLowerCase()));
//        assertThat("Metode pembayaran salah", nameMetode.toLowerCase(), containsString(bank.toLowerCase()));
//        assertThat("Date In salah", lakukanPembayaranPage.getDateIn(), containsString(dateIn));
//        assertThat("Date Out salah", lakukanPembayaranPage.getDateOut(), containsString(dateOut));
//        assertThat("Nama Kota salah", lakukanPembayaranPage.getCityName().toLowerCase(), containsString(kota.toLowerCase()));
//        assertThat("Nama Hotel Salah", lakukanPembayaranPage.getHotelName().toLowerCase(), containsString(hotel.toLowerCase()));
//        assertThat("Nama Ruangan salah", lakukanPembayaranPage.getRoomName().toLowerCase(), containsString(ruangan.toLowerCase()));
//        assertThat("Harga Total salah", lakukanPembayaranPage.getTotalPrice().toLowerCase(), containsString(hargaTotal.toLowerCase()));
//        assertThat("Total Ruangan salah", lakukanPembayaranPage.getTotalRoom(), containsString(Integer.toString(totalRoom)));
//        assertThat("Total Tamu Salah", lakukanPembayaranPage.getTotalTamu(), containsString(Integer.toString(totalTamu)));
//        assertThat("Nama Pemesan Salah", lakukanPembayaranPage.getPemesanName(), containsString(name));
//        assertThat("Email Pemesan Salah", lakukanPembayaranPage.getPemesanEmail(), containsString(email));
//        assertThat("Nomor Telepon salah", lakukanPembayaranPage.getPemesanHanphone(), containsString(telp));
//        assertThat("Nama Tamu salah", lakukanPembayaranPage.getTamuName(), containsString(nameTamu));
//    }
//
//    @Test
//    public void testPembayaranGerai() throws Exception {
//        PemesananPage pemesananPage = new PemesananPage(getDriver());
//        HotelDetailPage hotelDetailPage = new HotelDetailPage(getDriver());
//        PembayaranPage pembayaranPage = new PembayaranPage(getDriver());
//        LakukanPembayaranPage lakukanPembayaranPage = new LakukanPembayaranPage(getDriver());
//        String name = getObj().getProperty("name1");
//        String telp = getObj().getProperty("telp1");
//        String email = getObj().getProperty("email1");
//        String nameTamu = getObj().getProperty("namaTamu");
//        String gerai = getObj().getProperty("gerai");
//        HotelPage hotelPage = new HotelPage(getDriver());
//        HomePage homePage = new HomePage(getDriver());
//        ResultSearchPage resultSearchPage = new ResultSearchPage(getDriver());
//        homePage.openHomePage();
//        homePage.cleanView();
//        homePage.clickHotel();
//        String kota = hotelPage.getKotaSelected();
//        hotelPage.clickCheckIn();
//        hotelPage.insertDateIn(dateIn);
//        hotelPage.clickCheckOut();
//        hotelPage.insertDateOut(dateOut);
//        hotelPage.clickJumlah();
//        hotelPage.clickPlusKamar(totalRoom);
//        hotelPage.clickPlusTamu(totalTamu);
//        hotelPage.clickBtnCariHotel();
//        String hotel = resultSearchPage.getFirstName(hotelOrder);
//        resultSearchPage.clickFirstHotel(hotelOrder);
//        String ruangan = hotelDetailPage.getRoomName(roomOrder);
//        hotelDetailPage.clickFirstPesan(roomOrder);
//        pemesananPage.inputName(name);
//        pemesananPage.inputTelpNumber(telp);
//        pemesananPage.inputEmail(email);
//        pemesananPage.inputTamu(nameTamu);
//        pemesananPage.clickBtnLanjutkan();
//        String hargaTotal = pembayaranPage.getTotalPrice();
//        pembayaranPage.clickTFGerai();
//        pembayaranPage.clickOpsiPembayaran();
//        pembayaranPage.clickGerai(gerai);
//        pembayaranPage.clickBtnBayar();
//        String namaGerai = lakukanPembayaranPage.getPembayaran();
//        String nameMetode = lakukanPembayaranPage.getMetodePembayaran();
//        assertThat("Nama Gerai pembayaran salah", namaGerai.toLowerCase(), containsString(gerai.toLowerCase()));
//        assertThat("Metode pembayaran salah", nameMetode.toLowerCase(), containsString(gerai.toLowerCase()));
//        assertThat("Date In salah", lakukanPembayaranPage.getDateIn(), containsString(dateIn));
//        assertThat("Date Out salah", lakukanPembayaranPage.getDateOut(), containsString(dateOut));
//        assertThat("Nama Kota salah", lakukanPembayaranPage.getCityName().toLowerCase(), containsString(kota.toLowerCase()));
//        assertThat("Nama Hotel Salah", lakukanPembayaranPage.getHotelName().toLowerCase(), containsString(hotel.toLowerCase()));
//        assertThat("Nama Ruangan salah", lakukanPembayaranPage.getRoomName().toLowerCase(), containsString(ruangan.toLowerCase()));
//        assertThat("Harga Total salah", lakukanPembayaranPage.getTotalPrice().toLowerCase(), containsString(hargaTotal.toLowerCase()));
//        assertThat("Total Ruangan salah", lakukanPembayaranPage.getTotalRoom(), containsString(Integer.toString(totalRoom)));
//        assertThat("Total Tamu Salah", lakukanPembayaranPage.getTotalTamu(), containsString(Integer.toString(totalTamu)));
//        assertThat("Nama Pemesan Salah", lakukanPembayaranPage.getPemesanName(), containsString(name));
//        assertThat("Email Pemesan Salah", lakukanPembayaranPage.getPemesanEmail(), containsString(email));
//        assertThat("Nomor Telepon salah", lakukanPembayaranPage.getPemesanHanphone(), containsString(telp));
//        assertThat("Nama Tamu salah", lakukanPembayaranPage.getTamuName(), containsString(nameTamu));
//    }
}
