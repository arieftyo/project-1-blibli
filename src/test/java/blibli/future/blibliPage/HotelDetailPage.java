package blibli.future.blibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class HotelDetailPage extends Utils {
    WebDriver webDriver;

//    public void scrollDown(WebDriver webDriver){
//        JavascriptExecutor jse = (JavascriptExecutor)webDriver;
//        jse.executeScript("window.scrollBy(0,1200)");
//    }


    public HotelDetailPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }


    public String getHotelName(){
        waiting("//div[@class='grid__col-8']//div[@class='hotel-name']", webDriver);
        String name = webDriver.findElement(By.xpath("//div[@class='grid__col-8']//div[@class='hotel-name']")).getText();
        return name;
    }

    public void clickFirstPesan(String roomOrder) throws Exception {
        waiting("//button[@class='see-room-button']", webDriver);
        int roomInt = Integer.parseInt(roomOrder);
        webDriver.findElement(By.xpath("//button[@class='see-room-button']")).click();
        Thread.sleep(5000);
        webDriver.findElement(By.xpath("(//button[@class='room-box-third-section-button'])[" + roomOrder +"]")).click();
        capturePass("Click Pesan", webDriver);
    }

    public String getRoomName(String roomOrder){
        waiting("(//div[@class='room-box-second-section-title'])[" + roomOrder + "]", webDriver);
        String roomName = webDriver.findElement(By.xpath("(//div[@class='room-box-second-section-title'])[" + roomOrder + "]")).getText();
        roomName = roomName.substring(roomName.indexOf(" ") + 1);
        return roomName;
    }

    public String getRoomPrice(String roomOrder){
        waiting("(//div[@class='room-box-third-section-price'])[" + roomOrder + "]", webDriver);
        String roomPrice = webDriver.findElement(By.xpath("(//div[@class='room-box-third-section-price'])[" + roomOrder + "]")).getText();
        return roomPrice;
    }


}
