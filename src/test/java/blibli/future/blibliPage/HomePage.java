package blibli.future.blibliPage;

import org.openqa.selenium.*;

public class HomePage extends Utils {
    WebDriver webDriver;

    public HomePage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void  openHomePage() throws Exception {
        webDriver.get("https://blibli.com");
        capturePass("Open Home Page", webDriver);
    }

    public boolean isImageBlibli(){
        return webDriver.findElement(By.xpath("//a[@class='router-link-exact-active router-link-active']//img")).isEnabled();
    }

    public void cleanView(){
        try {
            webDriver.findElement(By.xpath("//button[contains(text(),'Lain kali')]")).click();
        }
        catch (Exception e){

        }

        try {
            webDriver.switchTo().frame(webDriver.findElement(By.xpath("//iframe[@class='sp-fancybox-iframe adaptive-resolution']")));
            webDriver.findElement(By.xpath("//div[@id='close-button-1454703945249']")).click();

        }
        catch (Exception e){
            webDriver.switchTo().defaultContent();
        }

        try {
            webDriver.findElement(By.xpath("//div[contains(text(),'Lewati')]")).click();
        }
        catch (Exception e){

        }
    }

    public void clickLewati(){
        waiting("//button//div[contains(text(),'Lewati')]", webDriver);
        WebElement ele = webDriver.findElement(By.xpath("//button//div[contains(text(),'Lewati')]"));
        JavascriptExecutor executor = (JavascriptExecutor)webDriver;
        executor.executeScript("arguments[0].click();", ele);
    }

    public void clickLogin() throws Exception {
        webDriver.findElement(By.xpath("//button[@class='blu-btn btn__login b-outline b-small']"))
                    .click();
        capturePass("Click Login", webDriver);
    }

    public void clickHotel() throws Exception {
        waiting("//div[@class='item-label'][contains(text(),'Hotel')]", webDriver);
        webDriver.findElement(By.xpath("//div[@class='item-label'][contains(text(),'Hotel')]")).click();
        capturePass("Click Hotel", webDriver);
    }

}
