package blibli.future.blibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;

import java.util.List;

public class ResultSearchPage extends Utils {
    WebDriver webDriver;

    public ResultSearchPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void clickHargaTermurah() throws Exception {
        waiting("//div[contains(text(),'Harga Termurah')]", webDriver);
        webDriver.findElement(By.xpath("//div[contains(text(),'Harga Termurah')]")).click();
        capturePass("click harga termurah", webDriver);
    }

    public void clickHargaTermahal() throws Exception {
        waiting("//div[contains(text(),'Harga Termahal')]", webDriver);
        webDriver.findElement(By.xpath("//div[contains(text(),'Harga Termahal')]")).click();
        capturePass("Click Harga Termahal", webDriver);
    }

    public void clickReviewTerbaik() throws Exception {
        waiting("//div[contains(text(),'Review Terbaik')]", webDriver);
        webDriver.findElement(By.xpath("//div[contains(text(),'Review Terbaik')]")).click();
        capturePass("Click Review Terbaik", webDriver);
    }

    public double getFirstPrice() throws InterruptedException {
        String xpath = "(//div[@class='price'])[1]";
        waiting(xpath, webDriver);
        String price = webDriver.findElement(By.xpath(xpath)).getText();
        if (price.isEmpty()){
            Thread.sleep(2000);
            price = webDriver.findElement(By.xpath(xpath)).getText();
        }
        price = price.substring(price.indexOf("p") + 1);
        price = price.replace(".", "");
        double dPrice = Double.parseDouble(price);
        return dPrice;
    }

    public double getSecondPrice() throws InterruptedException {
        String xpath = "(//div[@class='price'])[2]";
        waiting(xpath, webDriver);
        String price = webDriver.findElement(By.xpath(xpath)).getText();
        if (price.isEmpty()){
            Thread.sleep(2000);
            price = webDriver.findElement(By.xpath(xpath)).getText();
        }
        price = price.substring(price.indexOf("p") + 1);
        price = price.replace(".", "");
        double dPrice = Double.parseDouble(price);
        return dPrice;
    }

    public double getThirdPrice() throws InterruptedException {
        String xpath = "(//div[@class='price'])[3]";
        waiting(xpath, webDriver);
        String price = webDriver.findElement(By.xpath(xpath)).getText();
        if (price.isEmpty()){
            Thread.sleep(2000);
            price = webDriver.findElement(By.xpath(xpath)).getText();
        }
        price = price.substring(price.indexOf("p") + 1);
        price = price.replace(".", "");
        double dPrice = Double.parseDouble(price);
        return dPrice;
    }

    public double getFirstReview(){
        int halfStarCount = 0;
        int fullStarCount = 0 ;
        double ratings ;
        try {
            List<WebElement> fullStar = webDriver.findElements(
                    By.xpath("(//span[@class='hotel-review-star'])[2]//i[@class='bli-circle-selected hotel-review-star-small-icon undefined']"));
            fullStarCount = fullStar.size();
        } catch (Exception e) {
            fullStarCount = 0;
        }
        try {
            List<WebElement> halfStar = webDriver.findElements(
                    By.xpath("(//span[@class='hotel-review-star'])[2]//i[@class='bli-circle-selected-half hotel-review-star-small-icon undefined']"));
            halfStarCount = halfStar.size();
        } catch (Exception e){
            halfStarCount = 0;
        }

        if (fullStarCount != 0 && halfStarCount != 0){
            ratings = (double) fullStarCount + ((double) halfStarCount/2);
        }
        else if (fullStarCount == 0 && halfStarCount !=0){
            ratings = (double) halfStarCount/2;
        }
        else if (halfStarCount == 0 && fullStarCount != 0){
            ratings = (double) fullStarCount;
        }
        else {
            ratings = 0;
        }
        return ratings;
    }

    public double getSecondReview(){
        int halfStarCount = 0;
        int fullStarCount = 0 ;
        double ratings ;
        try {
            List<WebElement> fullStar = webDriver.findElements(
                    By.xpath("(//span[@class='hotel-review-star'])[4]//i[@class='bli-circle-selected hotel-review-star-small-icon undefined']"));
            fullStarCount = fullStar.size();
        } catch (Exception e) {
            fullStarCount = 0;
        }
        try {
            List<WebElement> halfStar = webDriver.findElements(
                    By.xpath("(//span[@class='hotel-review-star'])[4]//i[@class='bli-circle-selected-half hotel-review-star-small-icon undefined']"));
            halfStarCount = halfStar.size();
        } catch (Exception e){
            halfStarCount = 0;
        }

        if (fullStarCount != 0 && halfStarCount != 0){
            ratings = (double) fullStarCount + ((double) halfStarCount/2);
        }
        else if (fullStarCount == 0 && halfStarCount !=0){
            ratings = (double) halfStarCount/2;
        }
        else if (halfStarCount == 0 && fullStarCount != 0){
            ratings = (double) fullStarCount;
        }
        else {
            ratings = 0;
        }
        return ratings;
    }

    public double getTenthReview(){
        int halfStarCount = 0;
        int fullStarCount = 0 ;
        double ratings ;
        try {
            List<WebElement> fullStar = webDriver.findElements(
                    By.xpath("(//span[@class='hotel-review-star'])[20]//i[@class='bli-circle-selected hotel-review-star-small-icon undefined']"));
            fullStarCount = fullStar.size();
        } catch (Exception e) {
            fullStarCount = 0;
        }
        try {
            List<WebElement> halfStar = webDriver.findElements(
                    By.xpath("(//span[@class='hotel-review-star'])[20]//i[@class='bli-circle-selected-half hotel-review-star-small-icon undefined']"));
            halfStarCount = halfStar.size();
        } catch (Exception e){
            halfStarCount = 0;
        }

        if (fullStarCount != 0 && halfStarCount != 0){
            ratings = (double) fullStarCount + ((double) halfStarCount/2);
        }
        else if (fullStarCount == 0 && halfStarCount !=0){
            ratings = (double) halfStarCount/2;
        }
        else if (halfStarCount == 0 && fullStarCount != 0){
            ratings = (double) fullStarCount;
        }
        else {
            ratings = 0;
        }
        return ratings;
    }

    public void dragMinPrice(){
        waiting("//div[@class='vue-slider-hover vue-slider-dot'][1]", webDriver);
        WebElement slider = webDriver.findElement(By.xpath("//div[@class='vue-slider-hover vue-slider-dot'][1]"));
        Actions move = new Actions(webDriver);
        Action action = (Action) move.dragAndDropBy(slider, 50, 0).build();
        action.perform();
    }

    public void dragMaxPrice(){
        waiting("//div[@class='vue-slider-hover vue-slider-dot'][2]", webDriver);
        WebElement slider = webDriver.findElement(By.xpath("//div[@class='vue-slider-hover vue-slider-dot'][2]"));
        Actions move = new Actions(webDriver);
        Action action = (Action) move.dragAndDropBy(slider, -50, 0).build();
        action.perform();
    }

    public void clickResetBtn() throws Exception {
        waiting("//div[@class='reset-btn']", webDriver);
        webDriver.findElement(By.xpath("//div[@class='reset-btn']")).click();
        capturePass("Click Btn Reset", webDriver);
    }

    public double getMinPrice(){
        String price = webDriver.findElement(By.xpath("//span[@class='value-min']")).getText();
        price = price.substring(price.indexOf("p") + 1);
        price = price.replace(".", "");
        double dPrice = Double.parseDouble(price);
        return dPrice;
    }

    public double getMaxPrice(){
        String price = webDriver.findElement(By.xpath("//span[@class='value-max']")).getText();
        price = price.substring(price.indexOf("p") + 1);
        price = price.replace(".", "");
        double dPrice = Double.parseDouble(price);
        return dPrice;
    }


    public String getFirstName(String hotelOrder){
        waiting("(//div[@class='hotel-name'])[" + hotelOrder + "]", webDriver);
        String name = webDriver.findElement(By.xpath("(//div[@class='hotel-name'])[" + hotelOrder + "]")).getText();
        name = name.substring(name.indexOf(" ") + 1);
        return name;
    }

    public void clickFirstHotel(String hotelOrder){
        int hotelInt = Integer.parseInt(hotelOrder);
        String xpath = "(//div[@class='hotel__item'])[" + hotelOrder + "]";
        waiting(xpath, webDriver);
        WebElement element = webDriver.findElement(By.xpath(xpath));
        if (hotelInt >= 3){
            scrollDown(webDriver, element);
        }
        element.click();
    }

    public String getKota(){
        String kota = webDriver.findElement(By.xpath("//div[@class='search-location']")).getText();
        return kota;
    }

    public String getDateIn(){
        String dateIn = webDriver.findElement(By.xpath("//div[5][@class='HotelRangeCalendar']//div[@class='box']//span")).getText();
        return dateIn;
    }

    public String getDataOut(){
        String dateOut = webDriver.findElement(By.xpath("//div[6][@class='HotelRangeCalendar']//div[@class='box']//span")).getText();
        return dateOut;
    }


}
