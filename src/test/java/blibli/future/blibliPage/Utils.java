package blibli.future.blibliPage;

import blibli.future.ExtentReport.ExtendTestManager;
import com.aventstack.extentreports.MediaEntityBuilder;
import org.openqa.selenium.*;
import org.openqa.selenium.io.FileHandler;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.File;
import java.io.IOException;

public class Utils {

    public String screenshot(WebDriver webDriver) throws IOException {
        String targetLocation = null;
        String testMethodName = String.valueOf(System.currentTimeMillis());
        String screenShotName = testMethodName + ".png";
        String fileSeperator = System.getProperty("file.separator");
        String reportsPath = System.getProperty("user.dir") + fileSeperator + "TestReport" + fileSeperator
                + "screenshots";
        String imageReader = "screenshots";
        File file = new File(reportsPath + fileSeperator); // Set
        // screenshots
        // folder
        if (!file.exists()) {
            if (file.mkdirs()) {
                System.out.println("Directory: " + file.getAbsolutePath() + " is created!");
            } else {
                System.out.println("Failed to create directory: " + file.getAbsolutePath());
            }

        }

        File screenshotFile = ((TakesScreenshot) webDriver).getScreenshotAs(OutputType.FILE);
        targetLocation = reportsPath + fileSeperator + fileSeperator + screenShotName;
        String imageLocation = imageReader + fileSeperator + fileSeperator + screenShotName;
        File targetFile = new File(targetLocation);
        System.out.println("Screen shot file location - " + screenshotFile.getAbsolutePath());
        System.out.println("Target File location - " + targetFile.getAbsolutePath());
        FileHandler.copy(screenshotFile, targetFile);
        return imageLocation;
    }

    public void capturePass(String detail, WebDriver webDriver) throws Exception {
        Thread.sleep(3000);
        ExtendTestManager.getTest().pass(detail,
                MediaEntityBuilder.createScreenCaptureFromPath(screenshot(webDriver)).build());;
    }


    public void waiting(String xpath, WebDriver webDriver){
        WebDriverWait wait = new  WebDriverWait(webDriver, 120);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath(xpath)));
    }

    public void scrollDown(WebDriver webDriver, WebElement Element){
        JavascriptExecutor js = (JavascriptExecutor) webDriver;
        js.executeScript("arguments[0].scrollIntoView();", Element);
    }

}
