package blibli.future.blibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class PembayaranPage extends Utils {
    WebDriver webDriver;

    public PembayaranPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public String getPembayaranPage(){
        waiting("//div[@class='payment__list']//p[@class='font-18']", webDriver);
        String pembayaran = webDriver.findElement(By.xpath("//div[@class='payment__list']//p[@class='font-18']")).getText();
        return pembayaran;
    }

    public String getIdentitasPemesan(){
        String identitas = webDriver.findElement(By.xpath("//div[@class='table-row line-height-normal']//div[2]")).getText();
        return identitas;
    }

    public String getNamaTamu(){
        String tamu = webDriver.findElement(By.xpath("//div[@class='table-row ']//div[2]")).getText();
        return tamu;
    }

    public void clickTFVirtualAccount() throws Exception {
        waiting("//li[contains(text(),'Virtual Account')]", webDriver);
        webDriver.findElement(By.xpath("//li[contains(text(),'Virtual Account')]")).click();
        capturePass("click Transfer Virtual Account", webDriver);
    }

    public void clickOpsiPembayaran() throws Exception {
        waiting("//div[@class='payment__tab-card']//button[@class='button button--big']",webDriver);
        webDriver.findElement(By.xpath("//div[@class='payment__tab-card']//button[@class='button button--big']")).click();
        capturePass("Click opsi pembayaran", webDriver);
    }

    public void clickTFGerai() throws Exception {
        waiting("//li[contains(text(),'Gerai')]", webDriver);
        webDriver.findElement(By.xpath("//li[contains(text(),'Gerai')]")).click();
        capturePass("Click Transfer Via Gerai", webDriver);
    }


    public void clickBankVA(String bankName) throws Exception {
        if (bankName.equals("BCA")){
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[contains(text(),'Bank BCA')]")));
            webDriver.findElement(By.xpath("//li[contains(text(),'Bank BCA')]")).click();
            System.out.println("lalalalala yeyeyeyeye");
        }
        else if (bankName == "Mandiri"){
            waiting("//li[contains(text(),'Bank Mandiri')]", webDriver);
            WebElement ele = webDriver.findElement(By.xpath("//li[contains(text(),'Bank Mandiri')]"));
            JavascriptExecutor executor = (JavascriptExecutor)webDriver;
            executor.executeScript("arguments[0].click();", ele);
        }
        capturePass("click Bank Virtual Account", webDriver);
    }

    public void clickGerai(String gerai){
        if (gerai.equals("Afla Group")){
            webDriver.findElement(By.xpath("//li[contains(text(),'Alfa Group')]")).click();
        }
        else if (gerai.toLowerCase().equals("indomaret")){
            webDriver.findElement(By.xpath("//li[contains(text(),'Indomaret')]")).click();
        }
        else if (gerai.toLowerCase().equals("pos indonesia")){
            WebDriverWait wait = new WebDriverWait(webDriver, 10);
            wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//li[contains(text(),'Pos Indonesia')]")));
            webDriver.findElement(By.xpath("//li[contains(text(),'Pos Indonesia')]")).click();
        }
    }

    public void clickBtnBayar() throws Exception {
        waiting("//button[contains(text(),'Bayar Sekarang')]", webDriver);
        webDriver.findElement(By.xpath("//button[contains(text(),'Bayar Sekarang')]")).click();
        capturePass("click Bayar sekarang button", webDriver);
    }

    public String getHotelName(){
        String hotelName = webDriver.findElement(By.xpath("(//div[@class='pricing']//div[2])[1]")).getText();
        return hotelName;
    }

    public String getRoomName(){
        String roomName = webDriver.findElement(By.xpath("(//div[@class='pricing']//div[3]//div[1])[1]")).getText();
        return roomName;
    }

    public String getDate(){
        String date = webDriver.findElement(By.xpath("(//div[@class='pricing']//div[3]//div[2])[1]")).getText();
        return getDate();
    }

    public String getDateIn(){
        String date = this.getDate();
        String dateIn = date.substring(0, 2);
        return dateIn;
    }

    public String getDateOut(){
        String date = this.getDate();
        String dateOut = date.substring(date.indexOf("- "), date.indexOf(" "));
        return dateOut;
    }

    public String getTotalPrice(){
        waiting("//div[@class='color--orange right']", webDriver);
        String totalPrice = webDriver.findElement(By.xpath("//div[@class='color--orange right']")).getText();
        return totalPrice;
    }

}
