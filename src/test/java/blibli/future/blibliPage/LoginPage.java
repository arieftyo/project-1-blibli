package blibli.future.blibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.FileNotFoundException;

public class LoginPage extends Utils {

    WebDriver webDriver;

    public LoginPage(WebDriver webDriver) throws FileNotFoundException {
        this.webDriver = webDriver;
    }



    public void enterUserEmail(String userEmail) throws Exception {
        waiting("//input[@type='email']", webDriver);
        webDriver.findElement(By.xpath("//input[@type='email']")).sendKeys(userEmail);
        capturePass("Insert Email", webDriver);
    }

    public void enterUserPassword(String password) throws Exception {
        waiting("//input[@type='password']", webDriver);
        webDriver.findElement(By.xpath("//input[@type='password']")).sendKeys(password);
        capturePass("Enter password", webDriver);
    }

    public void clickMasuk() throws Exception {
        waiting("//button[@class='blu-btn b-full-width b-warning']", webDriver);
        webDriver.findElement(By.xpath("//button[@class='blu-btn b-full-width b-warning']")).click();
        capturePass("Click Button Login", webDriver);
    }
}
