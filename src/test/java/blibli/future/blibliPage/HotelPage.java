package blibli.future.blibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
public class HotelPage extends Utils {

    WebDriver webDriver;

    public HotelPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public void clickSingaporeHotel() throws Exception {
        waiting("//*[contains(text(),'SINGAPORE')]", webDriver);
        webDriver.findElement(By.xpath("//*[contains(text(),'SINGAPORE')]")).click();
        capturePass("Click hotel di Singapore", webDriver);
    }

    public void clickKualalumpurHotel() throws Exception {
        waiting("//*[contains(text(),'KUALA LUMPUR')]", webDriver);
        webDriver.findElement(By.xpath("//*[contains(text(),'KUALA LUMPUR')]")).click();
        capturePass("Click hotel di Kuala Lumpur", webDriver);
    }

    public void clickTokyoHotel() throws Exception {
        waiting("//*[contains(text(),'TOKYO')]", webDriver);
        webDriver.findElement(By.xpath("//*[contains(text(),'TOKYO')]")).click();
        capturePass("Click hotel di Tokyo", webDriver);
    }

    public void clickBangkokHotel() throws Exception {
        waiting("//*[contains(text(),'BANGKOK')]", webDriver);
        webDriver.findElement(By.xpath("//*[contains(text(),'BANGKOK')]")).click();
        capturePass("Click hotel di Bangkok", webDriver);
    }

    public void clickJakartaHotel() throws Exception {
        waiting("//*[contains(text(),'JAKARTA')]", webDriver);
        webDriver.findElement(By.xpath("//*[contains(text(),'JAKARTA')]")).click();
        capturePass("Click hotel di Jakarta", webDriver);
    }

    public void clickBaliHotel() throws Exception {
        waiting("//*[contains(text(),'BALI')]", webDriver);
        webDriver.findElement(By.xpath("//*[contains(text(),'BALI')]")).click();
        capturePass("Click hotel di Bali", webDriver);
    }

    public void clickYogyakartaHotel() throws Exception {
        waiting("//*[contains(text(),'YOGYAKARTA')]", webDriver);
        webDriver.findElement(By.xpath("//*[contains(text(),'YOGYAKARTA')]")).click();
        capturePass("Click hotel di Yogyakarta", webDriver);
    }

    public void clickSurabayaHotel() throws Exception {
        waiting("//*[contains(text(),'SURABAYA')]", webDriver);
        webDriver.findElement(By.xpath("//*[contains(text(),'SURABAYA')]")).click();
        capturePass("Click hotel di Surabaya", webDriver);
    }

    public void clickBandungHotel() throws Exception {
        waiting("//div[contains(text(),'Bandung')]", webDriver);
        webDriver.findElement(By.xpath("//div[contains(text(),'Bandung')]")).click();
        capturePass("Click hotel di Bandung", webDriver);
    }

    public void insertKota(String kota) throws Exception {
        int kotaLength = kota.length();
        for (int i = 0; i < kotaLength; i++){
            String a = String.valueOf(kota.charAt(i));
            webDriver.findElement(By.xpath("//input[@class='search-bar-input']")).sendKeys(a);
            Thread.sleep(100);
        }
        capturePass("Insert Kota", webDriver);
    }

    public void selectKota() throws Exception {
        String xpath = "//div[@class='input hotel__search-form-autocomplete table-cell']//li[1]";
        waiting(xpath, webDriver);
        webDriver.findElement(By.xpath(xpath)).click();
        capturePass("Select Kota", webDriver);
    }

    public String getKotaSelected(){
        String kota = webDriver.findElement(By.xpath("//input[@class='search-bar-input']")).getAttribute("value");
        return kota.toLowerCase();
    }

    public void clickCheckIn() throws Exception {
        waiting("//div[@class='input hotel__search-form-calendar table-cell'][1]", webDriver);
        webDriver.findElement(By.xpath("//div[@class='input hotel__search-form-calendar table-cell'][1]")).click();
        capturePass("Click Check-in", webDriver);

    }

    public void insertDateIn(String date_in) throws Exception {
        String xpath = "//div[2][@class='input hotel__search-form-calendar table-cell']//div[1][@class='pika-lendar']//td[@data-day='";
        xpath = xpath.concat(date_in);
        xpath = xpath.concat("']");
        webDriver.findElement(By.xpath(xpath)).click();
        capturePass("Insert Date-in", webDriver);

    }

    public String getDateInSelected(){
        String dateIn = webDriver.findElement(By.xpath("//div[2][@class='input hotel__search-form-calendar table-cell']//span")).getText();
        return dateIn.toLowerCase();
    }

    public void clickCheckOut() throws Exception {
        webDriver.findElement(By.xpath("//div[@class='input hotel__search-form-calendar table-cell'][2]")).click();
        capturePass("Click Check-out", webDriver);
    }

    public void insertDateOut(String date_out) throws Exception {
        String xpath = "//div[3][@class='input hotel__search-form-calendar table-cell']//div[1][@class='pika-lendar']//td[@data-day='";
        xpath = xpath.concat(date_out);
        xpath = xpath.concat("']");
        waiting(xpath, webDriver);
        webDriver.findElement(By.xpath(xpath)).click();
        capturePass("Insert Date-Out", webDriver);
    }

    public String getDateOutSelected(){
        String date_out = webDriver.findElement(By.xpath("//div[3][@class='input hotel__search-form-calendar table-cell']//span")).getText();
        return date_out.toLowerCase();
    }

    public void clickJumlah() throws Exception {
        waiting("//div[@class='input hotel__search-form-combo table-cell']//div[@class='drop-down-menu']//div//div[contains(@class,'homepage-drop-down')]", webDriver);
        webDriver.findElement(By.xpath("//div[@class='input hotel__search-form-combo table-cell']//div[@class='drop-down-menu']//div//div[contains(@class,'homepage-drop-down')]")).click();
        capturePass("Click Jumlah", webDriver);
    }

    public String getTotalPeopleInput(){
        String people = webDriver.findElement(By.xpath("//div[1][@class='grid__row room--wrapper']//div[@class='room--label']")).getText();
        return people;
    }

    public String getTotalPeopleOutput(){
       String people =  webDriver.findElement(By.xpath("//div[contains(text(),' kamar')]")).getText();
       people = people.substring(0, people.indexOf("-"));
       return people.toLowerCase();
    }

    public String getTotalRoomiInput(){
        String room = webDriver.findElement(By.xpath("//div[2][@class='grid__row room--wrapper']//div[@class='room--label']")).getText();
        return room;
    }
    public String getTotalRoomOutput(){
        String  room = webDriver.findElement(By.xpath("//div[contains(text(),' kamar')]")).getText();
        room = room.substring(room.indexOf("-") + 1);
        return room.toLowerCase();
    }

    public void clickPlusTamu(int jumlah) throws Exception {
        jumlah = jumlah - 1;
        while (jumlah > 0) {
            webDriver.findElement(By.xpath("//div[@class='number-picker-section']//div[1]//div[1]//div[2]//div[1]//div[3]//button[1]")).click();
            jumlah--;
        }
        capturePass("Click Plus Tamu", webDriver);
    }

    public void clickMinusTamu() throws Exception {
        webDriver.findElement(By.xpath("//div[@class='number-picker-section']//div[1]//div[1]//div[2]//div[1]//div[1]//button[1]")).click();
        capturePass("Click Minus Tamu", webDriver);
    }

    public void clickPlusKamar(int jumlah) throws Exception {
        jumlah = jumlah - 1;
        while (jumlah > 0) {
            webDriver.findElement(By.xpath("//body//div[@class='homepage-drop-down active']//div//div//div[2]//div[1]//div[2]//div[1]//div[3]//button[1]")).click();
            jumlah--;
        }
        capturePass("Click Plus Kamar", webDriver);
    }

    public void clickMinusKamar() throws Exception {
        webDriver.findElement(By.xpath("//body//div[@class='homepage-drop-down active']//div//div//div[2]//div[1]//div[2]//div[1]//div[1]//button[1]")).click();
        capturePass("Click Minus Kamar", webDriver);
    }

    public void clickBtnCariHotel() throws Exception {
        webDriver.findElement(By.xpath("//button[@class='button button--orange table-cell button--full']")).click();
        capturePass("Click Cari Hotel", webDriver);
    }

    public void deleteKota(int length) throws Exception {
        while (length > 0) {
            webDriver.findElement(By.xpath("//input[@placeholder='Kota atau nama hotel']")).sendKeys(Keys.BACK_SPACE);
            length--;
        }
        capturePass("Delete Kota", webDriver);
    }

}
