package blibli.future.blibliPage ;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class PemesananPage extends Utils {
    WebDriver webDriver;

    public PemesananPage(WebDriver webDriver) {
        this.webDriver = webDriver;
    }

    public String getIsiDataTitle() {
        waiting("//li[@class='step active']//div[2]", webDriver);
        String title = webDriver.findElement(By.xpath("//div[@class='hotel__guest-form-title font-18 margin-bottom-15 font-bold']")).getText();
        return title;
    }

    public void inputName(String name) throws Exception {
        waiting("(//div[@class='text__input'])[1]//input", webDriver);
        webDriver.findElement(By.xpath("(//div[@class='text__input'])[1]//input")).sendKeys(name);
        webDriver.findElement(By.xpath("//div[@class='hotel__guest-form-content']")).click();
        capturePass("Input Nama", webDriver);
    }

    public String getName() {
        String name = webDriver.findElement(By.xpath("(//div[@class='text__input'])[1]//input")).getAttribute("value");
        return name;
    }

    public String getErrorName() {
        waiting("(//div[@class='text__input'])[1]//div[@class='font-red']", webDriver);
        String error = webDriver.findElement(By.xpath("(//div[@class='text__input'])[1]//div[@class='font-red']")).getText();
        return error;
    }

    public void inputTelpNumber(String telp) throws Exception {
        waiting("(//div[@class='text__input'])[2]//input",webDriver);
        webDriver.findElement(By.xpath("(//div[@class='text__input'])[2]//input")).sendKeys(telp);
        webDriver.findElement(By.xpath("//div[@class='hotel__guest-form-content']")).click();
        capturePass("Inpurt Telephone Number", webDriver);
    }

    public String getErrorTelp() {
        waiting("(//div[@class='text__input'])[2]//div[@class='font-red']", webDriver);
        String error = webDriver.findElement(By.xpath("(//div[@class='text__input'])[2]//div[@class='font-red']")).getText();
        return error;
    }

    public String getTelpNumber() {
        String telpNumber = webDriver.findElement(By.xpath("(//div[@class='text__input'])[2]//input")).getAttribute("value");
        return telpNumber;
    }

    public void inputEmail(String email) throws Exception {
        waiting("(//div[@class='text__input'])[3]//input", webDriver);
        webDriver.findElement(By.xpath("(//div[@class='text__input'])[3]//input")).sendKeys(email);
        webDriver.findElement(By.xpath("//div[@class='hotel__guest-form-content']")).click();
        capturePass("Input Email", webDriver);
    }

    public String getEmail() {
        String email = webDriver.findElement(By.xpath("(//div[@class='text__input'])[3]//input")).getAttribute("value");
        return email;
    }

    public String getErrorEmail() {
        waiting("(//div[@class='text__input'])[3]//div[@class='font-red']", webDriver);
        String error = webDriver.findElement(By.xpath("(//div[@class='text__input'])[3]//div[@class='font-red']")).getText();
        return error;
    }

    public void inputTamu(String name) throws Exception {
        waiting("//input[@placeholder='Isi Nama Lengkap Tamu']", webDriver);
        webDriver.findElement(By.xpath("//input[@placeholder='Isi Nama Lengkap Tamu']")).sendKeys(name);
        webDriver.findElement(By.xpath("//div[@class='hotel__guest-form-content']")).click();
        capturePass("Input Tamu", webDriver);
    }
    public String getTamu(){
        String tamu =  webDriver.findElement(By.xpath("//input[@placeholder='Isi Nama Lengkap Tamu']")).getAttribute("value");
        return tamu;
    }

    public String getErrorTamu(){
        waiting("//div[@class='font-red']", webDriver);
        String error = webDriver.findElement(By.xpath("//div[@class='font-red']")).getText();
        return error;
    }

    public void clickBtnSamaPemesan() throws Exception {
        webDriver.findElement(By.xpath("//div[@class='same__as-contact']//input[@type='checkbox']")).click();
        capturePass("Click Nama Tamu sama dengan pemesan", webDriver);
    }

    public void clickBtnLanjutkan(){
        WebDriverWait wait = new WebDriverWait(webDriver, 10);
        WebElement element = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//button[contains(text(),'Lanjutkan Pemesanan')]")));
        element.click();
    }

    public String getHotelName(){
        String hotelName = webDriver.findElement(By.xpath("//div[@class='name']")).getText();
        return hotelName;
    }

    public String getRoomName(){
        String roomName = webDriver.findElement(By.xpath("//div[@class='font-bold margin-top-10']")).getText();
        return roomName;
    }

    public String getDateIn(){
        String dateIn = webDriver.findElement(By.xpath("//div[@class='hotel__info']//div[contains(text(),' Malam')]")).getText();
        dateIn = dateIn.substring(0, dateIn.indexOf("-"));
        return dateIn;
    }

    public String getDateOut(){
        String dateOut = webDriver.findElement(By.xpath("//div[@class='hotel__info']//div[contains(text(),' Malam')]")).getText();
        dateOut = dateOut.substring(dateOut.indexOf("-")+1);
        return dateOut;
    }

    public String getRoomPrice(){
        String price = webDriver.findElement(By.xpath("//div[@class='right']")).getText();
        return price;
    }

    public String getTotalPrice(){
        String totalPrice = webDriver.findElement(By.xpath("//div[@class='right color--orange']")).getText();
        return totalPrice;
    }
}
