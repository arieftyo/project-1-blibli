package blibli.future.blibliPage;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LakukanPembayaranPage extends Utils {

    WebDriver webDriver;

    public LakukanPembayaranPage(WebDriver webDriver){
        this.webDriver = webDriver;
    }

    public String getPembayaran(){
        waiting("//div[@class='label description']//div[2]//b", webDriver);
        String pembayaran = webDriver.findElement(By.xpath("//div[@class='label description']//div[2]//b")).getText();
        return pembayaran;
    }

    public String getMetodePembayaran(){
        waiting("(//div[@class='hotel__order-detail'])[2]//div[@class='pricing section'][1]//div[@class='table-row'][1]//div[2]", webDriver);
        String pembayaran = webDriver.findElement(By.xpath("(//div[@class='hotel__order-detail'])[2]//div[@class='pricing section'][1]//div[@class='table-row'][1]//div[2]")).getText();
        return pembayaran;
    }

    public String getTotalPrice(){
        String totalPrice = webDriver.findElement(By.xpath("//div[@class='content right']//div[@class='pricing section'][2]//div[@class='font-bold right price-value']")).getText();
        return totalPrice;
    }

   public String getHotelName(){
        String hotelName = webDriver.findElement(By.xpath("//div[@class='hotel-section section']//div[@class='name']")).getText();
        return hotelName;
   }

   public String getCityName(){
        String cityName = webDriver.findElement(By.xpath("//div[@class='hotel-section section']//div[@class='location']")).getText();
        return cityName;
   }

   public String getDateIn(){
        String dateIn = webDriver.findElement(By.xpath("(//div[@class='pricing section'][1]//div[@class='table-cell font-bold'][1])[1]")).getText();
        return dateIn;
   }

   public String getDateOut(){
        String dateOut = webDriver.findElement(By.xpath("(//div[@class='pricing section'][1]//div[@class='table-cell font-bold'][1])[2]")).getText();
        return dateOut;
   }

   public String getRoomName(){
        String roomName = webDriver.findElement(By.xpath("(//div[@class='pricing section'][1]//div[@class='table-cell font-bold'][1])[3]")).getText();
        return roomName;
   }

   public String getTotalRoom(){
        String totalRoom = webDriver.findElement(By.xpath("(//div[@class='pricing section'][1]//div[@class='table-cell font-bold'][1])[4]")).getText();
        return totalRoom;
   }

   public String getTotalTamu(){
        String totalTamu = webDriver.findElement(By.xpath("(//div[@class='pricing section'][1]//div[@class='table-cell font-bold'][1])[5]")).getText();
        return totalTamu;
   }

   public String getPemesanName(){
        String pemesanName = webDriver.findElement(By.xpath("(//div[@class='pricing section'][2]//div[@class='table-cell font-bold'][1])[1]")).getText();
        return pemesanName;
   }

   public String getPemesanEmail(){
        String pemesanEmail = webDriver.findElement(By.xpath("(//div[@class='pricing section'][2]//div[@class='table-cell font-bold'][1])[2]")).getText();
        return pemesanEmail;
   }

   public String getPemesanHanphone(){
        String pemesanHandphone = webDriver.findElement(By.xpath("(//div[@class='pricing section'][2]//div[@class='table-cell font-bold'][1])[3]")).getText();
        return pemesanHandphone;
   }

   public String getTamuName(){
        String tamuName = webDriver.findElement(By.xpath("(//div[@class='pricing section'][3]//div[@class='table-cell font-bold'][1])[1]")).getText();
        return  tamuName;
   }

}
